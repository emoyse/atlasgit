#!/usr/bin/bash
# pass the location of a clean repo when you execute this
# should run this inside atlasgit/clean_repo/stomp_history

CLEANREPO=$1
CURRENTDIR=`pwd`

#Just going to assume that the clean repo is on the correct branch
for FILE in `cat list.txt`
do 
if [[ -e $CLEANREPO/$FILE ]]; then
  cp $CLEANREPO/$FILE .
else
  echo "Warning $CLEANREPO/$FILE is missing."
fi
done

