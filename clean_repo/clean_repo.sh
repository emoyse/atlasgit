#!/usr/bin/bash

# Assuming you will run this in a directory which contains atlasgit 
# i.e. the command you'll type will be:
# atlasgit/clean_repo/cleanrepo.sh

CURRDIR=`pwd`

RUN_FILTER="YES"
MAX_FILE_SIZE="10M"

date
echo
echo ++++ Starting repository cleanup in $CURRDIR

if [ ! $RUN_FILTER ]; then
    echo "WARNING! Not running the filter-tree step."
fi

echo ++++ 1. Copying BFG locally.
echo
if [ ! -e "bfg-1.13.0.jar" ]; then
  wget http://repo1.maven.org/maven2/com/madgag/bfg/1.13.0/bfg-1.13.0.jar
else
  echo Already have BFG locally. Not downloading.
  echo
fi

echo ++++ 2. Getting bare repo
echo 
git clone --mirror ssh://git@gitlab.cern.ch:7999/atlas/athena.git
# Temporarily use local clone
# git clone --mirror ../athena

echo "++++ 3. Trying to remove large (>$MAX_FILE_SIZE) files, using BFG."
echo
java -jar bfg-1.13.0.jar --strip-blobs-bigger-than $MAX_FILE_SIZE --protect-blobs-from master,21.0,21.1,21.2,21.3,21.6,21.9,21.0-TrigMC athena.git
cd athena.git
echo Now cleanup.
git reflog expire --expire=now --all && git gc --prune=now --aggressive
cd ..

echo ++++ 4. Trying to remove inappropriate language, using BFG.
echo
java -jar bfg-1.13.0.jar  --replace-text atlasgit/clean_repo/bannedphrases.txt  athena.git
echo Now cleanup.
cd athena.git
git reflog expire --expire=now --all && git gc --prune=now --aggressive
cd ..

# This directory needs to be removed:
# Tracking/TrkAlignment/TrkAlgebraUtils/MA27
echo ++++ 5. Remove directory we want gone, using BFG.
java -jar bfg-1.13.0.jar --delete-folders MA27 athena.git 
echo Now cleanup.
cd athena.git
git reflog expire --expire=now --all && git gc --prune=now --aggressive
cd ..

if [ ! $RUN_FILTER ]; then
    echo "Not running filter-branch in step 7, so instead delete files using BFG."
    # instead let's use BFG 
    # TODO!!! One problem here is (as usual) BFG doesn't take the path, just filename. So e.g. CMakeLists.txt can't be fixed.
    # java -jar bfg-1.13.0.jar --delete-files --protect-blobs-from master,21.0,21.1,21.2,21.3,21.6,21.9,21.0-TrigMC athena$
fi

echo ++++ 6. Get working copy from the bare repo
# Maybe better to do git config --bool core.bare false instead?
echo
mkdir -p LocalCopy/athena
cd LocalCopy/athena
cp -r ../../athena.git .git
git config --bool core.bare false
git checkout master # This is just to stop git thinking we've deleted everything.

echo "++++ 7. Stomp history (replace historic versions of files with newer ones from stomp_history directory) using filter-tree"
# Now, time for filterbranch
FILEDIR=$CURRDIR/atlasgit/clean_repo/stomp_history
# Now fix problematic files by copying local version over version in repo
# git filter-branch -f --index-filter "
#   for LINE in `cat $CURRDIR/atlasgit/clean_repo/stomp_history/list.txt` ;
#   do
#     echo $LINE;
#     FILE=`basename $LINE` ;
#     if [ -d `dirname $LINE` ];
#       then cp -f $FILEDIR/$FILE $LINE  ; git add $LINE ; fi ;
#   done " --tag-name-filter cat -- --all

if [ $RUN_FILTER ]; then
    git filter-branch -f --index-filter "$FILEDIR/replace_files.sh $CURRDIR" --tag-name-filter cat -- --all
else
    echo "WARNING! Not running the filter-tree step."
fi

# echo ++++ 8. Fix file permissions.
# git for-each-ref --shell   --format='git checkout -b %(refname) ; ${CURRDIR}/atlasgit/clean_repo/git-fix-permissions.py ; git commit -a -m "Fixing permissions." ; git push'   refs/heads/ > fix_files.txt
# source fix_files.txt

echo ++++ 8. Fix permissions of identical files
for branch in 21.0 21.1 21.2 21.3 21.6 21.9 master; do
    git checkout $branch
    $CURRDIR/atlasgit/clean_repo/git-fix-permissions.py
    nfiles=`git status --short | wc -l`
    git commit -a -F- <<EOF
Fix file permissions of identical files

Files with identical content (same SHA1) but different file permissions
cause confusing differences shown in GitLab. The file permissions of
$nfiles files were set to 644 (ATLINFR-2011).
EOF
done


echo ++++ 9. Final cleanup and push to server

echo 
echo "Now we need to manually run the copyright scripts, check for protected files (i.e. grep log for dirty file )"
echo "Once this is done, we can rename atlas/athena to atlas/athenaprivate_1, add licence statement to all branches, and import this cleaned repo."

# git remote rename origin old-origin
# git remote add origin ssh://git@gitlab.cern.ch:7999/atlas-repo-cleanup/imported-athena.git
# git push -u origin --all
# git push -u origin --tags


cd $CURRDIR/

echo Finished
date



